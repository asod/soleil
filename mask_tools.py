import numpy as np

# Function to find streak columns in the mask
def find_streak_columns(mask):
    streaks = []
    for col in range(mask.shape[1]):
        if np.all(mask[:, col]):
            streaks.append(col)
    return streaks

# Function to group consecutive columns
def group_consecutive_columns(columns):
    grouped_columns = []
    current_group = [columns[0]]
    
    for col in columns[1:]:
        if col == current_group[-1] + 1:
            current_group.append(col)
        else:
            grouped_columns.append(current_group)
            current_group = [col]
    
    grouped_columns.append(current_group)
    return grouped_columns

# Function to calculate fill values as the mean of nearest non-masked columns
def calculate_fill_values(data, mask):
    filled_data = data.copy()
    streak_columns = find_streak_columns(mask)
    grouped_streaks = group_consecutive_columns(streak_columns)
    
    for group in grouped_streaks:
        left_col = group[0] - 1
        right_col = group[-1] + 1
        
        # Find the nearest non-masked column to the left
        while left_col >= 0 and np.all(mask[:, left_col]):
            left_col -= 1
        
        # Find the nearest non-masked column to the right
        while right_col < data.shape[1] and np.all(mask[:, right_col]):
            right_col += 1
        
        if left_col >= 0 and right_col < data.shape[1]:
            left_values = data[:, left_col]
            right_values = data[:, right_col]
            fill_value = (left_values + right_values) / 2
        elif left_col >= 0:
            fill_value = data[:, left_col]
        elif right_col < data.shape[1]:
            fill_value = data[:, right_col]
        else:
            fill_value = np.zeros(data.shape[0])  # Fallback value if no non-masked column is found
        
        for col in group:
            filled_data[:, col] = fill_value
    
    return filled_data