import numpy as np
import h5py as h5
import os 
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from mask_tools import calculate_fill_values

fig_sizes = (6, 4)


''' Some functions for auto-ROI'''
# 2D Gaussian function
def gaussian_2d(x, y, x0, y0, xalpha, yalpha, amplitude, offset):
    return offset + amplitude * np.exp(-(((x - x0) / xalpha) ** 2 + ((y - y0) / yalpha) ** 2) / 2)

# Wrapper for fitting
def gaussian_fit(data, guess=None):
    def fit_func(xdata_tuple, x0, y0, xalpha, yalpha, amplitude, offset):
        (x, y) = xdata_tuple
        return gaussian_2d(x, y, x0, y0, xalpha, yalpha, amplitude, offset).ravel()

    x = np.arange(data.shape[1])
    y = np.arange(data.shape[0])
    x, y = np.meshgrid(x, y)
    xdata = np.vstack((x.ravel(), y.ravel()))

    if guess is None:
        initial_guess = (data.shape[1] // 2, data.shape[0] // 2, 20, 20, np.max(data), np.min(data))
    else: 
        initial_guess = (guess[0], guess[1], 20, 20, np.max(data), np.min(data))
    popt, _ = curve_fit(fit_func, xdata, data.ravel(), p0=initial_guess)
    return popt


class RXES:
    """
    A class to handle and process RXES (Resonant X-ray Emission Spectroscopy) data from NeXus (nxs) files.

    Parameters
    ----------
    filenames : list of str
        List of paths to NeXus (.nxs) files containing the scans.
    k : str, optional
        use Kalpha or K_beta calibrant.
    i0loc : str, optional
        Location of i0, only the last part of the path is considered ('APD1' or 'APD2').
        You can also normalize with 'RIXS_DIODE', which is NOT an I0, but it also works
        for normalizing XAS. 
    mask : np.array of shape (256, 1024) dtype bool (for the Merlin4x1 detector)
        True means pixel is filtered! 
    mask_style : str
        1. 'ruff': Quick threshold to get rid of hot pixels only,
                   on image 0 of scan
        2. 'nans': Using npy mask, filling with NaN
        3. 'avgs': Using npy mask, filling only hot pixels with NaN AND 
                   meaning streaks in column N, N + 1 
                   with the values of column N - 1 and N + 2 

    (Relevant) Attributes
    ---------------------
    images : np.ndarray, (n, l, 256, 1024)
        n = #nxs files, l = #scan points, (256, 1024) = detector shape
    roi : list, [roi_max, roi_min]  
        The vertical ROI in which to average the intensity.
        set automatically with RXES().set_roi or just overwrite
        with your own limits
    calib : np.ndarray
        Containing data from e.g. calib_Mn_Kalpha.txt
     
    """
    def __init__(self, filenames, k='alpha', i0loc='APD1', mask=None, mask_style='ruff'):
        self.fns = filenames  # list of filenames
        self.i0loc = i0loc  
        self.k  = k # alpha or beta, or a list [alpha, beta, ...] for many scans 
        self.mask = mask  # (256, 1024) np.array where True means pixel is filtered 
        self.mask_style = mask_style 

        self.e_inci = None #np.linspace(6528, 6582, 109)  # FIXME: READ FROM nxs!
        # init vars needed later
        self.nxs_files = [h5.File(fn,'r') for fn in filenames]
        self.images = None  # 
        self.roi = None
        self.calib = [] 
        self.rxes = None 
        # location of this file, for localizing calibrants 
        self.here = os.path.dirname(os.path.abspath(__file__))

    def read_data(self):
        self.read_images()
        self.read_meta()  # FIXME: rename, as it's also reading out SDD, which isn't meta.

    def read_images(self):
        raw_image = list()
        for nxs_file in self.nxs_files:
            this_image = np.expand_dims(nxs_file['root_spyc_config1d_RIXS_0001/scan_data/merlin_4x1_image'],
                                    axis=0)
            raw_image.append(this_image)
        self.images = np.concatenate(raw_image, axis=0)

    def read_meta(self):
        ''' Read metadata: 
            i0, SDD (silicon drift detector), and it's deadtime
        '''
        i0 = []
    
        for nxs in self.nxs_files:
            try:
                i0.append(np.array(nxs['root_spyc_config1d_RIXS_0001']['GALAXIES']['scan_record'][self.i0loc]))
            except:
                raise RuntimeError(f'I0 not found for {nxs}')
        self.i0 = i0

        trunk = '/root_spyc_config1d_RIXS_0001/scan_data'
        # silicon drift detector
        sdd = [np.array(nxs_file[trunk]['xspchannel01']) for nxs_file in self.nxs_files]
        self.sdd = sdd
        # and its deadtime
        deadt = [np.array(nxs_file[trunk]['xspdeadtime01']) for nxs_file in self.nxs_files]
        self.deadt = deadt

        e_inci =[np.array(a['root_spyc_config1d_RIXS_0001/GALAXIES/scan_record/MotorPos1']) 
                 for a in self.nxs_files]
        self.e_inci = e_inci

    def load_calibrant(self, fname=None):
        ''' Load calibrant txt to convert pixel-val to emitted energy'''
        
        if fname is None:
            ks = self.k
            if type(ks) == str:
                ks = [ks]
            for k in ks: 
                pth = os.path.join(self.here, 'calibrants') 
                fname = os.path.join(pth, f'calib_Mn_K{k}.txt')
                self.calib.append(np.genfromtxt(fname))

    def apply_mask(self, this_image):
        ''' Applies mask to 2D detector. Three options:
            (0. No mask)
            1. 'ruff' Quick threshold to get rid of hot pixels only,
                on image 0 of scan
            2. 'nans' Using npy mask, filling with NaN
            3. 'avgs' Using npy mask, filling only hot pixels with NaN AND 
               meaning streaks in column N, N + 1 
               with the values of column N - 1 and N + 2 
        '''
        if self.mask is None:  # option 0
            return this_image
        if self.mask_style == 'ruff':
            hot_pix = self.images[0, 0, :, : ] > 10  # WIP: hardcoded threshold. BAD
            ma = np.ma.masked_array(this_image.astype(float),
                                         mask=hot_pix)
            return ma.filled()  
        elif self.mask_style == 'nans':
            assert self.mask is not None, 'load a mask!'
            ma = np.ma.masked_array(this_image.astype(float),
                                    mask=self.mask, fill_value=np.nan)
            return ma.filled()
        elif self.mask_style == 'avgs':
            assert self.mask is not None, 'load a mask!'
            ma = np.ma.masked_array(this_image.astype(float),
                                    mask=self.mask, fill_value=0)
            
            avgd = calculate_fill_values(ma, self.mask)
            # good but THEN you need to remove the hot pixels.. 
            #hot_pix = self.images[0, 0, :, : ] > 10
            #ma_avgd = np.ma.masked_array(avgd, mask=hot_pix, fill_value=np.NaN)
            return avgd.filled(0) # fill the HPs with 0
        else:
            raise RuntimeError('mask_style not understood. Choose ruff, nans, or avgs')

    def auto_roi(self, im_num, guess=None, roi_width=2, file_num=0, clim=[1, 100], plot=True):
        ''' Autodetect Horizontal ROI, by fitting a 2D Gauss, then set roi to 2 x std (default)
            im_num : int
                image number (scan point)
            guess : [x, y] list of int
                Initial guess of the center for the 2D Gaussian fit. If nothing is 
                specified, it will use the center of the detector. This can fail
            roi_width : float
                Set the ROI to center - x_std * std, center + x_std * std
            file_num : int
                number of scan in your list of loaded scans
            clim : [min, max]
                color scale limits if plotting
            plot : bool
                Whether or not to plot the resulting ROI
        '''
        if self.images is None:
            self.read_images()
        
        this_image = self.images[file_num, im_num, :, :]
        filtered_image = self.apply_mask(this_image)
        
        # gaussian fit doesn't want NaN
        filtered_image = np.nan_to_num(filtered_image, nan=0)
        x0, y0, x_std, y_std, amp, offset = gaussian_fit(filtered_image, guess)
             
        # Make sure roi is always formatted the right way
        self.roi = sorted([int(round(y0 - roi_width * y_std)), 
                           int(round(y0 + roi_width * y_std))], reverse=True)

        if plot:
            ax = self.plot_image(im_num, file_num, clim)           
            ax.plot(x0, y0, 'rx')

    def plot_image(self, im_num, file_num=0, clim=[1, 100], ax=None):
        ''' Plots image from 2D detector
            im_num : int
                image from scan point im_num
            file_num : int 
                Which file in self.nxs_files to calculate RXES for
            clim : [min, max] list
                color scale limits
            ax : mpl axes object to plot into. If None is given
                it will be created. 
        '''
        if ax is None:
            fig, ax = plt.subplots(1, 1, figsize=(6, 4))

        raw_im = self.images[file_num, im_num, :, :]
        mask = self.mask
        if mask is None:  # no mask then
            mask = np.zeros(raw_im.shape, bool)
            im = ax.imshow(raw_im)
        else:
            ma = np.ma.masked_array(raw_im.astype(float), 
                                    mask=mask, fill_value=np.nan)
            im = ax.imshow(ma.filled())        
        
        plt.colorbar(im, ax=ax, orientation='horizontal')
        im.set_clim(clim)
        if self.roi is not None:
            ax.axhline(self.roi[0], color='m')
            ax.axhline(self.roi[1], color='m')
        return ax 
    
    def calc_rxes(self, file_num=0, avg_method=np.nansum):
        ''' Calculate RXES 
            file_num : int 
                Which file in self.nxs_files to calculate RXES for
            avg_method : numpy method
                If you have no mask or the automask fails,
                you can try np.nanmedian which is less sensitive
                to outliers from hot pixels etc. 

                if mask_style = 'ruff', you NEED to do the median,
                so that is forced.
        '''
        if self.mask_style == 'ruff':
            avg_method = np.nanmedian
        if self.roi is None:
            self.auto_roi(im_num=-10, plot=False)  # this is a bit of a roulette
        lines = []
        roi = self.roi
        for j in range(self.images.shape[1]):
            this_image = self.images[file_num, j, :, :]
            filled_image = self.apply_mask(this_image)
            y = avg_method(filled_image[roi[1]:roi[0], :], axis=0)
            lines.append(y)
        imshape = self.images.shape
        rxes = np.reshape(lines, (imshape[1], imshape[-1]))
        self.rxes = rxes 

    def plot_rxes(self, file_num=0, transfer=False):
        ''' Plot an RXES map of the Emitted Energy vs the Incident Energy
            file_num: int
                Which file in your list of nxs files to plot from
            transfer: bool
                Set to True if you want a maybe-correct (or maybe not)
                Energy transfer plot
        '''
        if self.rxes is None:
            self.calc_rxes()
        rxes = self.rxes

        e_inci = self.e_inci[file_num]
        e_emit = np.arange(1024)  
        calib = None 
        if len(self.calib) > 0:
            calib = self.calib[file_num]
            fit = np.polyfit(calib[:, 1], calib[:, 0], 1)
            pixel_grid = np.arange(1024, 0, -1)
            e_emit = np.polyval(fit, pixel_grid)

        X, Y = e_inci, e_emit
        em_unit = '(pixel)' if calib is None else '(eV)'
        if transfer:
            Y = e_inci - e_emit[:, np.newaxis]
            X, _ = np.meshgrid(e_inci, e_emit)
            em_unit = '$\Delta E$ (eV)' 

        fig, ax = plt.subplots(1, 1, figsize=(9, 4))
        ax.contourf(X, Y, rxes.T, cmap='viridis')
        em_quantity = '' if transfer else 'Emitted Energy '
        ax.set_ylabel(f'{em_quantity} {em_unit}')
        ax.set_xlabel('Incident Energy (eV)')
        fig.tight_layout()

    def plot_sdd(self, im_num, file_num=0, ax=None):
        ''' Use to check the region in which to sum the 
            flourescence intensity. 

            If you have peaks from other stuff in your sample
            than the element you're probing, exclude those.
        '''
        if self.sdd is None:
            self.read_meta()
        e_sdd = np.arange(self.sdd[file_num].shape[1])  * 10
        if ax is None:
            fig, ax = plt.subplots(1, 1, figsize=fig_sizes)
        ax.plot(e_sdd, self.sdd[file_num][im_num])
        ax.set_xlabel('Energy (eV)')
        ax.set_ylabel('Counts')
        return ax

    def calc_xas(self, region, region_calib=None, file_num=0):
        ''' Calculate XAS from XDD detector.
            region : [min, max]
                Which region to sum the fluorescence in, normalized
                to I0
            region_calib : [min, max]
                If specified, this region will be normalized
                to the sum in this region instead of I0. If you
                have other fluorescence peaks that don't change 
                during your scan, you can try this. But it's a bit
                dodgy. 
        '''
        e_sdd = np.arange(self.sdd[0].shape[1])  * 10 # x10 to get eVs

        mask = np.zeros(len(e_sdd), bool)
        mask[(e_sdd > region[0]) & (e_sdd < region[1])] = True

        fl_summed = np.sum(self.sdd[file_num][:, mask], axis=1)

        if region_calib is None:  # Use I0 for normalization
            xas = (fl_summed  / (1 - self.deadt[file_num] / 100)) / self.i0[file_num]
        else:  # use the region of a static peak defined in region_calib
            mask_norm = np.zeros(len(e_sdd), bool)
            mask_norm[(e_sdd > region_calib[0]) & (e_sdd < region_calib[1])] = True
            nm_summed = np.sum(self.sdd[file_num][:, mask_norm], axis=1)
            xas = (fl_summed  / nm_summed)
        self.xas = xas 

    def plot_xas(self, file_num=0, ax=None, **kwargs):
        if self.xas is None:
            raise RuntimeError('Run calc_xas first')
        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=fig_sizes)
        ax.plot(self.e_inci[file_num], self.xas, **kwargs)
        ax.set_xlabel('Incident Energy (eV)')
        ax.set_ylabel('XAS')
        return ax 

   




        
        
    



        



        




