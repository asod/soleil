
# RXES and XAS Online Plotting / Analysis for Soleil GALAXIES

The `RXES` class is designed to handle and process Resonant X-ray Emission Spectroscopy (RXES) data from NeXus (.nxs) files. This README will guide you through the setup, usage, and key functionalities of the `RXES` class.

Check the info on the classes by looking at their docstrings, e.g. `?rxes.calc_rxes`.

Also check the `examples.ipynb` for some very simple tutorials. 

### Init Parameters

- `filenames`: List of paths to NeXus (.nxs) files containing the scans.  
- `k`: Use `Kalpha` or `Kbeta` calibrant. Default is `'alpha'`.  
- `i0loc`: Location of i0, only the last part of the path is considered (`'APD1'` or `'APD2'`). Default is `'APD1'`.  
- `mask` : `np.ndarray` of shape (256, 1024) dtype bool (for the Merlin4x1 detector)  
    Mask used in masking out out bad pixels. True means pixel is filtered!  
    If no mask is entered, the 'ruff' masking will be employed to remove hot pixels (see below)
- `mask_style` : `str`  
        1. 'ruff': Quick threshold to get rid of hot pixels only, on image 0 of scan.  
        2. 'nans': Using npy mask, filling with NaN.  
        3. 'avgs': Using npy mask, filling only hot pixels with NaN AND meaning streaks in column N, N + 1 with the values of column N - 1 and N + 1. Kinda too fancy for its own good, so handle with care. Ideally you do _not_ want to 'create' data-points like this.


### Example
Here we assume that the file `'500_STO_0002.nxs'` is in the same directory as your python script/notebook using the class.


```python
from analysis.rxes import RXES

# Initialize the RXES class
rxes = RXES(['500_STO_0002.nxs'], k='alpha')
```

## Attributes

- `images`: `np.ndarray` of shape `(n, l, 256, 1024)` where `n` is the number of .nxs files, `l` is the number of scan points, and `(256, 1024)` is the detector shape.
- `roi`: List containing `[roi_max, roi_min]`. The vertical ROI in which to average the intensity.
- `calib`: List for containing calibration data for each scan you loaded.
- `i0`: Incident beam intensity.
- `sdd`: Silicon Drift Detector data.
- `deadt`: Deadtime corrections for the detector.
- `e_inci`: Incident energy values. Or, if something else was scanned, the values of that scanned motor
- `xas`: X-ray Absorption Spectroscopy data.

## Methods

### `read_data()`

Reads the images and metadata from the provided NeXus files.

```python
rxes.read_data()
```

### `read_images()`

Reads the raw images from the NeXus files.

### `read_meta()`

Reads the metadata including i0, SDD (silicon drift detector), and its deadtime.

### `load_calibrant(fname=None)`

Loads the calibrant text file to convert pixel values to emitted energy.

- `fname`: (Optional) The file name of the calibrant text file. If `None`, it defaults to `calib_Mn_K{self.k}.txt`.

```python
rxes.load_calibrant()
```

### `auto_roi(im_num, file_num=0, clim=[1, 100], filt_thresh=50, plot=True)`

Automatically determines the region of interest (ROI) by fitting a 2D Gaussian.

- `im_num`: Image number to use for ROI determination.
- `file_num`: (Optional) File number, corresponding to the index of the list of your loaded nxs files. Default is `0`.
- `clim`: (Optional) Color limits for the plot. Default is `[1, 100]`.
- `filt_thresh`: (Optional) Threshold for filtering hot pixels in the ROI determination. Not necessary if you have used a mask
- `plot`: (Optional) Whether to plot the image and ROI. Default is `True`.

```python
rxes.auto_roi(50, filt_thresh=200)
```

### `plot_image(im_num, file_num=0, clim=[1, 100], image=None)`

Plots a specific image with optional clim and ROI indicators.

- `im_num`: Image number to plot.
- `file_num`: (Optional) File number. Default is `0`.
- `clim`: (Optional) Color limits for the plot. Default is `[1, 100]`.
- `image`: (Optional) Image data to plot.

### `calc_rxes()`

Calculates the resonant emission spectrum using the ROI.

### `plot_rxes(file_num=0, ax=None)`

Plots the resonant emission spectrum.

- `file_num`: (Optional) File number to plot. Default is 0.
- `ax`: (Optional) Matplotlib axes object to plot on.

### `plot_sdd(im_num, file_num=0, ax=None))`

Plots the SDD (Silicon Drift Detector) data to check where to sum the fluorescence intensity.

- `im_num`: Image (scan point) number to plot.
- `file_num`: (Optional) File number. Default is `0`.
- `ax`: (Optional) Matplotlib axes object to plot on.


### `calc_xas(region, region_calib=None, file_num=0)`

Calculates the XAS spectrum based on a defined region.

- `region`: List containing `[ev_min, ev_max]` which defines the energy region to sum.
- `region_calib`: (Optional) Region of a static peak defined as `[ev_min, ev_max]`. If `None`, i0 is used for normalization.
- `file_num`: (Optional) File number. Default is `0`.

```python
rxes.calc_xas(region=[6500, 6600])
```

### `plot_xas(file_num=0, ax=None, **kwargs)`

Plots the calculated XAS spectrum. Ensure `calc_xas` has been run first.
- `file_num`: (Optional) File number to plot. Default is 0.
- `ax`: (Optional) Matplotlib axes object to plot on.
- `**kwargs`: Additional keyword arguments for plotting.

```python
rxes.plot_xas()
```

## Full Example

```python
from analysis.rxes import RXES

# Load a mask (if you have one)
mask = np.load('masks/merlin4x1_mask.npy')

# Initialize the RXES class
rxes = RXES(['500_STO_0002.nxs'], k='alpha', mask=mask, mask_style='nans')

# Load calibrant data
rxes.load_calibrant()

# Read data from the NeXus files
rxes.read_data()

# Automatically determine the ROI
rxes.auto_roi(50, filt_thresh=10)

# Calc and Plot the RXES map
rxes.calc_rxes()
rxes.plot_rxes()

# plot SDD at the last image in the scan to get region to sum for XAS
rxes.plot_sdd(-1)

# Calc and plot XAS
rxes.calc_xas([5500, 7740])
rxes.plot_xas()
```

This example initializes the `RXES` class with a NeXus file, loads the calibration data, reads the NeXus data, automatically determines the region of interest, and plots the RXES map.

## Dependencies

Ensure you have the following dependencies installed:

- numpy
- matplotlib
- h5py 
- scipy 

You can install these dependencies using pip:

```bash
pip install numpy matplotlib h5py scipy
```

# Data Channels at GALAXIES
Currently semi-hardcoded around the class, would be nicer to get from an external dict which could be updated/saved for 
each beamtime. 

### June 2024 LMO Beamtime:
1. `I0`: `'root_spyc_config1d_RIXS_0001/GALAXIES/scan_record'` and then `'APD1'` or `'APD2'` (which was used until it was changed)  
2. The scanned motor: `'root_spyc_config1d_RIXS_0001/GALAXIES/scan_record/MotorPos1'`. Seems this channel records whatever is being scanned. For 'production runs' this is normally the incident energy, which is reflected in the code. But for debugging scans (e.g. sample position, the values in this channel are simply replaced)  
3. 2D Detector: `'root_spyc_config1d_RIXS_0001/scan_data/merlin_4x1_image'`  
4. SDD: `'/root_spyc_config1d_RIXS_0001/scan_data/xspchannel01'` and `'xspdeadtime01'`
